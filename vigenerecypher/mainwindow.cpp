#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <string>
#include <QDebug>
#include <QString>
#include "cypher.h"
using namespace std;

///
/// \brief MainWindow::MainWindow- Function which sets up the MainWindow
///
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    setWindowTitle("Vigenere Cypher");
}

///
/// \brief MainWindow::~MainWindow- Default destructor
///
MainWindow::~MainWindow()
{
    delete ui;
}

///
/// \brief MainWindow::on_EncodeButton_clicked- Function that is called
/// when the encode button is pressed. The message and the keyword
/// that the user provides are placed into each variable accordingly then
/// the cypher function is called using these variables.
///
void MainWindow::on_EncodeButton_clicked()
{
   string message = ui->PlainTextTextEdit->toPlainText().toStdString();
   string key = ui->keyText->toPlainText().simplified().toStdString() ;
   ui->CypherTextEdit->setText(QString::fromStdString(cypher(message, key)));
}

///
/// \brief MainWindow::on_DecodeButton_clicked- Function that is called
/// when the decode button is pressed. The ciphertext and the keyword
/// that the user provides are placed into each variable accordingly
/// then the decypher function is called using these variables.
///
void MainWindow::on_DecodeButton_clicked()
{
    string message = ui->CypherTextEdit->toPlainText().toStdString();
    string key = ui->keyText->toPlainText().simplified().toStdString() ;
    ui->PlainTextTextEdit->setText(QString::fromStdString(decypher(message,key))) ;
}

#include <QApplication>
#include "mainwindow.h"

void test_cypher1();
void test_cypher2();

int main(int argc, char *argv[])
{
    //test_cypher1();
    test_cypher2();
    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    return a.exec();
}

#Lab. 8: Estructuras de repetición - Vigenere Cypher

<img src="http://i.imgur.com/DYSdjlN.png?1" width="215" height="174">  <img src="http://i.imgur.com/TEk9bMp.png?1" width="215" height="174">  <img src="http://i.imgur.com/3PV1IiK.png?1" width="215" height="174">


<p> </p>

Una de las ventajas de utilizar programas de computadoras es que podemos realizar tareas repetitivas fácilmente. Los ciclos como `for`, `while`, y `do-while` son  estructuras de control que nos permiten repetir un conjunto de instrucciones. A estas estructuras también se les llama *estructuras de repetición*. En la experiencia de laboratorio de hoy utilizarás una estructura de repetición sencilla para completar una aplicación que cifre un mensaje.

##Objetivos:

1. Aplicar estructuras de repetición para cifrar un mensaje.

2. Aplicar operaciones aritméticas con caracteres.

##Pre-Lab:

Antes de llegar al laboratorio debes:

1. haber repasado los conceptos básicos relacionados a estructuras de repetición.

2. haber repasado conceptos básicos de la clase `string` de C++,las funciones `length`, `toupper` y `push_back`, `isalpha` y las operaciones aritméticas con caracteres.

3. haber visto el video sobre el "Ceasar Cypher" del Khan Academy, colocado en <https://www.youtube.com/watch?v=sMOZf4GN3oc>.

4. haber visto el video sobre el "Vigenere Cypher", colocado en <https://www.youtube.com/watch?v=9zASwVoshiM>.

5. haber estudiado los conceptos e instrucciones para la sesión de laboratorio.

6. haber tomado el [quiz Pre-Lab 8](http://moodle.ccom.uprrp.edu/mod/quiz/view.php?id=7149) (recuerda que el quiz Pre-Lab puede tener conceptos explicados en las instrucciones del laboratorio).



##Criptografía 

La *criptografía* es un área que estudia la teoría y los métodos utilizados para proteger información de modo que personas que no estén autorizadas no puedan tener acceso a ella. Un *sistema criptográfico* es un sistema en donde un *cifrador* transforma la información (*mensaje*) en un texto cifrado inentendible para las personas no autorizadas a verlo. Las personas autorizadas a ver el mensaje usan un *descifrador* para transformar el texto cifrado en el mensaje original.

###El Cifrado César (Ceasar Cypher)

El Cifrado César es una técnica muy simple de cifrado por sustitución. Se dice que el sistema se basa en el sistema utilizado por Julio César, líder militar y político de Roma en años antes de Cristo, para comunicarse con sus generales.

La técnica transforma un mensaje en texto cifrado sustituyendo cada letra del mensaje por la letra que se encuentra a un número dado de posiciones más adelante. Esto puede pensarse como un desplazamiento ("shift") del alfabeto. El diagrama de la Figura 1 representa un desplazamiento de 3 espacios. La letra 'B' es sustituida por la letra 'E'.

<div align='center'><img src="http://i.imgur.com/DYSdjlN.png?1" width="400" height="200" alt="New Project" /></div>

<div align='center'><b>Figura 1.</b> Cifrador César con desplazamiento de 3 espacios.</div>

<p></p>

Por ejemplo, la palabra "ARROZ" quedaría cifrada como "DUURC". Nota que, con este desplazamiento de 3 espacios, la letra Z es sustituida por la letra 'C'.  Cuando el desplazamiento se pasa de la letra 'Z' comenzamos nuevamente en la letra 'A'. Esto se llama un desplazamiento cíclico. La Figura 2  muestra un desplazamiento cíclico de 8 espacios.

<div align='center'><img src="http://i.imgur.com/MZB8k1j.png?1" width="300" height="300" alt="New Project" /></div>

<div align='center'><b>Figura 2.</b> Disco para cifrador César; muestra un desplazamiento de 8 espacios.</div>

<p></p>

####Operador módulo

Podemos pensar que a cada letra le asignamos un número comenzando con el 0, que será asignado a la letra 'A'. Asumiendo que usamos un alfabeto con 26 letras (inglés), de la 'A' a la 'Z' tendremos los números del 0 al 25. Para hacer el desplazamiento cíclico, cada vez que nuestro desplazamiento nos dé una letra que corresponda a un número mayor que 25, tomamos el residuo al dividir por 26 y usamos la letra que corresponda a ese residuo. Nota que tomar el residuo al dividir por 26 funciona también para números entre 0 y 25, ?puedes explicar por qué?

El residuo al dividir dos números enteros se puede obtener utilizando el operador de *módulo*: `%`.

Volviendo al ejemplo de la palabra "ARROZ", a la letra 'Z' le corresponde el número 25; al hacer un desplazamiento de 3 espacios, sumamos 3 a 25 y tomamos el residuo al dividir el resultado por 26: `(25 + 3) % 26`. Esto nos dá 2, que corresponde a la letra 'C'. 

El proceso de arriba funciona si podemos asociar las letras de la `A` a la `Z` con los números del `0` al `25`. Esto se logra utilizando el valor numérico de los caracteres. Como en  el código ASCII el valor de las letras 'A' a la 'Z' va desde 65 hasta 90,  necesitamos hacer un ajuste en el cómputo. 

El pseudo-código de la Figura 3 hace el ajuste en el valor numérico de un caracter `c`, le hace un desplazamiento de `d` unidades al caracter, y luego le asigna  el caracter correspondiente al resultado del desplazamiento.

```
    Inputs: a string of plaintext, a shift amount d
    Output: a Ceasar Cyphered string

    1. cypheredText = ""
    2. for each character c in plaintext:
           c = ascii(c) - ascii('A')  # map c from [A,Z] to [0,25]
           c = ( c + d ) % 26         # shift c by d units
           c = ascii(c) + ascii('A')  # map c from [0,25] to [A,Z]
           cypheredText.append(c) 
    3. return cypheredText 
```

<b>Figura 3.</b> Pseudo-código para cifrar utilizando un cifrador César.

<p></p>

El cifrado César no es muy seguro ya que puede romperse fácilmente con un análisis de frecuencia. Por ejemplo, se sabe que en el idioma inglés la letra `e` es la letra más frecuente en un texto. Si buscamos la letra que más se repite en un texto cifrado con un cifrador César, lo más probable es que esa fue la letra que se sustituyó por la `e`; esto nos dice cuál fue el desplazamiento utilizado.

###El Cifrado Vigenere 

El cifrado Vigenere es un método de cifrado "polialfabético" en donde se usa una serie de cifrados César distintos cuyo desplazamiento está determinado por una palabra clave. Este cifrado también es susceptible a ataques pero es más fuerte que el cifrado César.

El cifrado Vigenere utiliza una palabra clave que determinará el desplazamiento que se le hará a cada letra del mensaje. Supongamos que el mensaje es "COSAS" y la palabra clave es "MENOR" (ambos tienen el mismo largo). La letra `C` se parea con la letra `M`, la letra `O` se parea con la letra `E`, la `S` con la `N`, etcétera, como en la figura de abajo.




| mensaje | C | O | S | A | S |
|---------|---|---|---|---|---|
| clave   | M | E | N | O | R |

<b>Figura 4.</b> Alineamiento del mensaje con la palabra clave.


<p> </p>

Si asumimos que la letra `A` corresponde a un desplazamiento de `0` espacios, entonces la letra `M` corresponde a un desplazamiento de `12` espacios, la `E` a un desplazamiento de `4` espacios, etc. Utilizando la palabra clave "MENOR", el cifrador Vigenere hará un desplazamiento de `12` espacios a la letra `C`, un desplazamiento de  `4` espacios a la letra `O`, etc., hasta obtener la palabra cifrada "OSFOJ" como se muestra en la Figura 5. Nota que tanto la letra `C` como la letra `A` fueron sustituidas por la letra `O`. Esto hace que el análisis de frecuencia sea un poco más difícil.


| mensaje         | C | O | S | A | S |
|-----------------|---|---|---|---|---|
| clave           | M | E | N | O | R |
| mensaje cifrado | O | S | F | O | J  |

<b>Figura 5.</b> Alineamiento del mensaje con la palabra clave y cifrado.

<p> </p>

Podemos visualizar un cifrador Vigenere utilizando una tabla como la de la Figura 6. Nota que lo que hace es usar un cifrador César con un desplazamiento diferente para cada letra del mensaje.

<div align='center'><img src="http://i.imgur.com/3PV1IiK.png?1" width="400" height="400" alt="New Project" /></div>

<div align='center'><b>Figura 6.</b> Tabla para cifrador Vigenere.</div>

<p></p>

Si la palabra clave es más corta que el mensaje, entonces repetimos la palabra clave tantas veces como haga falta, pareando letras del mensaje con letras de la palabra clave como se hace en la Figura 7.

|     mensaje     | A | L | G | U | N | A | S |   | C | O | S | A | S |
|-----------------|---|---|---|---|---|---|---|---|---|---|---|---|---|
| clave           | M | E | N | O | R | M | E |   | N | O | R | M | E |
| mensaje cifrado | M | P | T | I | E | M | W |   | P | C | J | M | W |


<b>Figura 7.</b> Alineamiento del mensaje con la palabra clave y cifrado.

<p> </p>

###Funciones que utilizaremos hoy:

El programa que estarás modificando en la sesión de hoy utiliza los siguientes métodos de la clase `string`:

* `length`: Devuelve el largo de un objeto de la clase `string`; esto es, `length` devuelve el número de caracteres que tiene el "string". Se utiliza escribiendo `.length()` después del nombre del objeto.

* `push_back`: Este método recibe un valor como argumento y lo añade al final de un vector. Se utiliza escribiendo `.push_back(elValor)` después del nombre del objeto. Por ejemplo, para añadir el caracter 'a' a  un objeto de la clase `string` llamado  `cadena`, escribimos `cadena.push_back('a')`.

También utilizaremos las funciones:

* `toupper`: Esta función recibe como argumento un caracter y devuelve el caracter en mayúscula. Por ejemplo, para cambiar el caracter 'a' a mayúscula se utiliza `toupper('a')`.

* `isalpha`: Esta función recibe como argumento un caracter y devuelve un valor distinto de cero ("true") si el caracter es una letra y cero ("false") si el caracter no es una letra. Por ejemplo, `isalpha(3)` devuelve "false" pero `isalpha(f)` devuelve "true".

##Sesión de laboratorio:

En esta experiencia de laboratorio completarás una aplicación para cifrar un mensaje de texto utilizando el cifrado Vigenere.




###Ejercicio 1


En este ejercicio completarás la aplicación para cifrar un mensaje de texto 
que solo contiene letras utilizando una palabra clave que también consiste solo de letras y que tiene el mismo largo del mensaje. 

**Instrucciones**

1. Abre un terminal y escribe el comando `git clone https://bitbucket.org/eip-uprrp/lab08-repetitions-vigenerecypher.git` para descargar la carpeta `Lab08-Repetitions-VigenereCypher` a tu computadora.

2.  Haz doble "click" en el archivo `VigenereCypher.pro` para cargar este proyecto a Qt.  

3. El archivo `cypher.cpp` es donde estarás añadiendo código. En este archivo, la función `cypher` recibe un mensaje y una clave y devuelve el mensaje cifrado por el cifrador Vigenere. Tu tarea es completar la función de cifrado, asumiendo que tanto el mensaje como la clave consisten solo de letras y que ambos tienen el mismo largo. 
 
    El código debe verificar si el mensaje y la clave tienen el mismo largo; si no lo tienen, el mensaje cifrado será "CLAVE INVALIDA". El programa debe implementar el cifrador Vigenere para ir cifrando cada letra del mensaje utilizando la clave. Para simplificar el código, tu programa debe cambiar todas las letras a mayúsculas. Solo debes utilizar las funciones mencionadas en la sección anterior.

4. Al terminar tu código, ve a la función `main` y descomenta la invocación a la función de prueba unitaria  `test_cypher1` para que verifiques tu programa.



###Ejercicio 2


En este ejercicio modificarás el código que creaste para el Ejercicio 1 de modo que la aplicación pueda cifrar cualquier mensaje de texto utilizando una palabra clave de cualquier largo.

**Instrucciones**

1. Comenta nuevamente la prueba unitara  `test_cypher1`. También comenta la función `cyper` que completaste en el Ejercicio 1, para que crees una nueva función `cypher`.

2. Escribe el código de la función `cypher` para que, al igual que en el Ejercicio 1, reciba un mensaje y una clave y devuelva el mensaje cifrado por el cifrador Vigenere. En esta ocasión, el mensaje y la clave pueden tener cualquier tipo de caracteres y cualquier largo (!mayor o igual a 1!). 

    El programa debe implementar el cifrador Vigenere para ir cifrando cada letra del mensaje utilizando la clave.  Para simplificar el código, tu programa debe cambiar todas las letras a mayúsculas. Si alguno de los caracteres no es una letra, el cifrador no lo cambia. Solo debes utilizar las funciones mencionadas en la sección anterior.

3. Al terminar tu código, ve a la función `main` y descomenta la invocación a la función de prueba unitaria `test_cypher2` para que verifiques tu programa.

4. Una vez te asegures de que tu código funciona bien, entrega el archivo `cypher.cpp` utilizando el [enlace en Moodle](http://moodle.ccom.uprrp.edu/mod/assignment/view.php?id=7563).

###Ejercicio 3 (opcional)

Comenta las versiones anteriores de la función `cypher` y crea una tercera función `cypher` modificando la anterior de manera que tanto el mensaje como la clave puedan contener cualquier caracter (número, símbolo, letras). Tu nueva función  `cypher` debe distinguir entre mayúsculas y minúsculas; es decir, una letra minúscula se cifra a otra letra minúscula.

ADVERTENCIA: El descifrador no funcionará para el código del Ejercicio 3.

##Referencias

http://www.nctm.org/uploadedImages/Classroom_Resources/Lesson_Plans/



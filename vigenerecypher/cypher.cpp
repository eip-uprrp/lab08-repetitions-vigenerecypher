#include <string>
#include <cassert>
#include <iostream>
#include <QDebug>

#include "cypher.h"

using namespace std;

///
/// \brief cypher- Function that recieves the plaintext message
/// and the keyword provided by the user and encrypts the message
///
/// \param message- Message to be encrypted provided by the user
///
/// \param key- keyword that the user provides, used to encrypt plain text
///
///
#include <iostream>

string cypher(string message, string key){
    string cypheredMsg = "";
    int    keyCtr = 0;
    char   c, d;


    for(unsigned int i = 0 ; i < message.length(); i++){

        if(isalpha(key[keyCtr])){

            if(isalpha(message[i])){
                c = toupper(message[i]) ;
                d = toupper(key[keyCtr]) ;
                c = (c + d - 2*'A') % 26 + 'A' ;
            }
            else
                c = message[i];

            keyCtr = (keyCtr + 1) % key.length() ;
            cypheredMsg.push_back(c);
        }
        else {
            return "MENSAJE O CLAVE INVALIDA";
        }
    }

    return cypheredMsg ;

}





void test_cypher1() {
    string plainTextSt = "Erase";
    string keySt = "torTu";
    assert(decypher(cypher(plainTextSt, keySt), keySt) == "ERASE");
    
    plainTextSt = "ERAS";
    keySt = "UPRR";
    assert(decypher(cypher(plainTextSt, keySt), keySt) == "ERAS");

    plainTextSt = "Largo";
    keySt = "Maslargo";
    assert(cypher(plainTextSt, keySt)== "MENSAJE O CLAVE INVALIDA");

    plainTextSt = "L@rgo";
    keySt = "Largo";
    assert(cypher(plainTextSt, keySt) == "MENSAJE O CLAVE INVALIDA");
    
    cout << "Passed unit test!" << endl;
}


void test_cypher2() {
    string plainTextSt = "Erase una vez";
    string keySt = "UPRRPCC@M";
    assert(cypher(plainTextSt, keySt) == "MENSAJE O CLAVE INVALIDA");

    plainTextSt = "Erase una vez";
    keySt = "torTuga";
    assert(decypher(cypher(plainTextSt, keySt), keySt) == "ERASE UNA VEZ");

    plainTextSt = "Er@se una ve$";
    keySt = "UPRRPCCOM";
    assert(decypher(cypher(plainTextSt, keySt), keySt) == "ER@SE UNA VE$");

    cout << "Passed unit test!" << endl;
}



///
/// \brief decypherCyclic - Function that deciphers Vigenere ciphertext,
/// receives the ciphertext and the keyword and returns the plaintext.
/// Works for messages and keys of different lengths.
///
/// \param message- ciphertext to be deciphered, provided by the user
///
/// \param key- keyword provided by the user, used to decipher the message
///
/// This function is obfuscated on purpose to disencourage students from trying to
/// copy the cypher function from it.

#define FIUHED if(
#define PIOIWE isalpha(
#define z341 string
#define u83 return
#define H_H_H int
#define if_ toupper
#define retur length()
#define q else
z341 decypher(string a, string r){
    z341 f = "";
    H_H_H    j = 0;
    char   d, c;

    for(H_H_H i=0 ; i < (H_H_H) a.retur; i++){

        FIUHED PIOIWE r[j])){
FIUHED PIOIWE a[i])){
c=if_(a[i]) -'A' ;d
=  if_(r[j])
- 'A';
c = (
c + 26 - d)
% 26 + 'A' ;
}
            q c = a[i];
j = (j + 1) % r.retur ;
f.push_back(c);} else { u83 "MENSAJE O CLAVE INVALIDA";
        }
    }

    u83 f ;
}
